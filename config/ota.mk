ifeq ($(CUSTOM_BUILD_TYPE), OFFICIAL)

CUSTOM_OTA_VERSION_CODE := android_10

CUSTOM_PROPERTIES += \
    org.euphoriaos.ota.version_code=$(CUSTOM_OTA_VERSION_CODE)

#PRODUCT_PACKAGES += \
#    Updates

PRODUCT_COPY_FILES += \
    vendor/aosp/config/permissions/org.euphoriaos.ota.xml:system/etc/permissions/org.euphoriaos.ota.xml

endif
